<?php

namespace Drupal\education_erp\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Profile type entity.
 *
 * @ConfigEntityType(
 *   id = "profile_type",
 *   label = @Translation("Profile type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\education_erp\ProfileTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\education_erp\Form\ProfileTypeForm",
 *       "edit" = "Drupal\education_erp\Form\ProfileTypeForm",
 *       "delete" = "Drupal\education_erp\Form\ProfileTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\education_erp\ProfileTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "profile_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "profile",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/node/add/profile_type/{profile_type}",
 *     "add-form" = "/node/add/profile_type/add",
 *     "edit-form" = "/node/add/profile_type/{profile_type}/edit",
 *     "delete-form" = "/node/add/profile_type/{profile_type}/delete",
 *     "collection" = "/node/add/profile_type"
 *   }
 * )
 */
class ProfileType extends ConfigEntityBundleBase implements ProfileTypeInterface {

  /**
   * The Profile type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Profile type label.
   *
   * @var string
   */
  protected $label;

}
