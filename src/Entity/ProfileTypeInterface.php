<?php

namespace Drupal\education_erp\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Profile type entities.
 */
interface ProfileTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
