<?php

namespace Drupal\education_erp\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Profile entities.
 *
 * @ingroup education_erp
 */
interface ProfileInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Profile name.
   *
   * @return string
   *   Name of the Profile.
   */
  public function getName();

  /**
   * Sets the Profile name.
   *
   * @param string $name
   *   The Profile name.
   *
   * @return \Drupal\education_erp\Entity\ProfileInterface
   *   The called Profile entity.
   */
  public function setName($name);

  /**
   * Gets the Profile creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Profile.
   */
  public function getCreatedTime();

  /**
   * Sets the Profile creation timestamp.
   *
   * @param int $timestamp
   *   The Profile creation timestamp.
   *
   * @return \Drupal\education_erp\Entity\ProfileInterface
   *   The called Profile entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Profile revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Profile revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\education_erp\Entity\ProfileInterface
   *   The called Profile entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Profile revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Profile revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\education_erp\Entity\ProfileInterface
   *   The called Profile entity.
   */
  public function setRevisionUserId($uid);

}
