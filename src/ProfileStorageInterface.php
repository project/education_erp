<?php

namespace Drupal\education_erp;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\education_erp\Entity\ProfileInterface;

/**
 * Defines the storage handler class for Profile entities.
 *
 * This extends the base storage class, adding required special handling for
 * Profile entities.
 *
 * @ingroup education_erp
 */
interface ProfileStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Profile revision IDs for a specific Profile.
   *
   * @param \Drupal\education_erp\Entity\ProfileInterface $entity
   *   The Profile entity.
   *
   * @return int[]
   *   Profile revision IDs (in ascending order).
   */
  public function revisionIds(ProfileInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Profile author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Profile revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
