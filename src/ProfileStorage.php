<?php

namespace Drupal\education_erp;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\education_erp\Entity\ProfileInterface;

/**
 * Defines the storage handler class for Profile entities.
 *
 * This extends the base storage class, adding required special handling for
 * Profile entities.
 *
 * @ingroup education_erp
 */
class ProfileStorage extends SqlContentEntityStorage implements ProfileStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(ProfileInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {profile_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {profile_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
