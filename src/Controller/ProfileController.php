<?php

namespace Drupal\education_erp\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\education_erp\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ProfileController.
 *
 *  Returns responses for Profile routes.
 */
class ProfileController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Profile revision.
   *
   * @param int $profile_revision
   *   The Profile revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($profile_revision) {
    $profile = $this->entityTypeManager()->getStorage('profile')
      ->loadRevision($profile_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('profile');

    return $view_builder->view($profile);
  }

  /**
   * Page title callback for a Profile revision.
   *
   * @param int $profile_revision
   *   The Profile revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($profile_revision) {
    $profile = $this->entityTypeManager()->getStorage('profile')
      ->loadRevision($profile_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $profile->label(),
      '%date' => $this->dateFormatter->format($profile->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Profile.
   *
   * @param \Drupal\education_erp\Entity\ProfileInterface $profile
   *   A Profile object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(ProfileInterface $profile) {
    $account = $this->currentUser();
    $profile_storage = $this->entityTypeManager()->getStorage('profile');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $profile->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all profile revisions") || $account->hasPermission('administer profile entities')));
    $delete_permission = (($account->hasPermission("delete all profile revisions") || $account->hasPermission('administer profile entities')));

    $rows = [];

    $vids = $profile_storage->revisionIds($profile);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\education_erp\ProfileInterface $revision */
      $revision = $profile_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $profile->getRevisionId()) {
          $link = $this->l($date, new Url('entity.profile.revision', [
            'profile' => $profile->id(),
            'profile_revision' => $vid,
          ]));
        }
        else {
          $link = $profile->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.profile.revision_revert', [
                'profile' => $profile->id(),
                'profile_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.profile.revision_delete', [
                'profile' => $profile->id(),
                'profile_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['profile_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
