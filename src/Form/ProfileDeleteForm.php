<?php

namespace Drupal\education_erp\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Profile entities.
 *
 * @ingroup education_erp
 */
class ProfileDeleteForm extends ContentEntityDeleteForm {


}
